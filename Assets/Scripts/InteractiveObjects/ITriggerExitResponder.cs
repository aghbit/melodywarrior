
using System;
using UnityEngine;


namespace AssemblyCSharp
{
    public interface ITriggerExitResponder
    {
        void OnPlayerExitTrigger(Transform p);
    }
}

