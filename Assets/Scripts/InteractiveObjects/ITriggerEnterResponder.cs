
using System;
using UnityEngine;


namespace AssemblyCSharp
{
    public interface ITriggerEnterResponder
    {
        void OnPlayerEnterTrigger(Transform p);
    }
}

