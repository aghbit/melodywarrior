using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class BeatTicker : MonoBehaviour, IBeatResponder
{
    private MusicManager musicManager;
    
    public void OnBeat(float timeOfBeat, float beatInterval)
    {
        GetComponent<AudioSource>().Play();
    }
    
    protected void Start () 
    {
        musicManager = MusicManager.Instance;
        musicManager.AddNewResponder(this,1.0f);
    }
    
    protected void Update () 
    {
        
    }
}
