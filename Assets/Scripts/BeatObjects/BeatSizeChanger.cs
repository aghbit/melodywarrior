using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class BeatSizeChanger : SizeChanger, IBeatResponder
{

    [SerializeField]
    private float beatInterval = 1f;
    private MusicManager musicManager;
	// Use this for initialization
	protected override void Start () 
    {
        base.Start();
        musicManager = MusicManager.Instance;
        musicManager.AddNewResponder(this,beatInterval);
    }

    public void OnBeat(float timeOfBeat,float beat)
    {
        TryChangingSize();
    }

    public void OnDestroy()
    {
        musicManager.RemoveResponder(this,beatInterval);
    }

}
