using UnityEngine;
using UnityEngine.UI;
using System;
namespace AssemblyCSharp
{
    public class HealthDisplay : MonoBehaviour
    {
        private Text healthNumberText;
        private GameManager gameManager;
        protected void Awake()
        {
            healthNumberText = transform.GetChild(0).GetComponent<Text>();
        }

        protected void Start () 
        {
            gameManager = GameManager.Instance;
            healthNumberText.text = gameManager.PlayerScript.PlayerHealthComponent.Health.ToString();
            gameManager.PlayerScript.PlayerHealthComponent.DamageTaken += DamageTaken;
        }

        private void SetDisplayedHealth(float newHealth)
        {
            string toDisplay = Mathf.RoundToInt(newHealth).ToString();
            healthNumberText.text = toDisplay;
        }

        private void DamageTaken(object Sender, DamageTakenEventArgs e)
        {
            SetDisplayedHealth(e.NewHealth);
        }
    }
}

