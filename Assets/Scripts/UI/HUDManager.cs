﻿using UnityEngine;
using System.Collections;

public class HUDManager : MonoBehaviour
{
    private static HUDManager instance;
    public static HUDManager Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<HUDManager>();               
                //if you want this to be persistent through scenes try adding some non-destroy code here
            }
            return instance;
        }      
    }
    Transform shotRating;
    public Transform ShotRating
    {
        get{return shotRating;}
        private set{shotRating = value;}
    }
    Transform chargeRating;
    public Transform ChargeRating
    {
        get{return chargeRating;}
        private set{chargeRating = value;}
    }
    Transform healthDisplay;
    public Transform HealthDisplay
    {
        get{return healthDisplay;}
        private set{healthDisplay = value;}
    }
    Transform ammoDisplay;
    public Transform AmmoDisplay
    {
        get{return ammoDisplay;}
        private set{ammoDisplay = value;}
    }
    Transform noteTracker;
    public Transform NoteTracker
    {
        get{return noteTracker;}
        private set{noteTracker = value;}
    }

    protected void Awake()
    {
        ShotRating = transform.Find("ShotRating");
        if(ShotRating == null)
        {
            Debug.LogWarning("ShotRating object not found in HUD");
        }
        ChargeRating = transform.Find("ChargeRating");
        if(ChargeRating == null)
        {
            Debug.LogWarning("ChargeRating object not found in HUD");
        }
        HealthDisplay = transform.Find("HealthDisplay");
        if(HealthDisplay == null)
        {
            Debug.LogWarning("HealthDisplay object not found in HUD");
        }
        AmmoDisplay = transform.Find("AmmoDisplay");
        if(AmmoDisplay == null)
        {
            Debug.LogWarning("AmmoDisplay object not found in HUD");
        }
        NoteTracker = transform.Find("NoteTracker");
        if(NoteTracker == null)
        {
            Debug.LogWarning("NoteTracker object not found in HUD");
        }

    }


	protected void Start () 
    {
          
	}


}
