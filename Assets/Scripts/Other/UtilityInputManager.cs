﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//this class is ugly but it's only temporary for cheat codes and debugging.
public class UtilityInputManager : MonoBehaviour
{
    private static UtilityInputManager instance;
    public static UtilityInputManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<UtilityInputManager>();
            }
            return instance;
        }
    }

    private GameManager gameManager;
    private MusicManager musicManager;
    protected void Start ()
    {
        gameManager = GameManager.Instance;
        musicManager = MusicManager.Instance;
    }

    protected void Update ()
    {
	    if(Input.GetKey(KeyCode.R))
        {
            gameManager.RestartLevel();
        }

        if(Input.GetKey(KeyCode.Escape))
        {
            gameManager.QuitGame();
        }

        float scrollInput = Input.GetAxis("Mouse ScrollWheel");
        if(scrollInput != 0.0f)
        {
            musicManager.Song.volume += scrollInput;
        }

        int SongSkipInput = 0;

        if(Input.GetKey(KeyCode.O))
        {
            SongSkipInput = -2;
        }
        else if (Input.GetKey(KeyCode.P))
        {
            SongSkipInput = 1;
        }
        if(SongSkipInput != 0)
        {
            float newTime = musicManager.ElapsedSongTime + SongSkipInput * 5.0f * Time.deltaTime;
            if (SongSkipInput == 1)
            {
                newTime -= Time.deltaTime;
            }
            else
            {
                newTime += Time.deltaTime;
            }
            if (newTime > musicManager.Song.clip.length)
            {
                newTime -= musicManager.Song.clip.length;
            }
            if(newTime < 0)
            {
                newTime = 0;
            }
            musicManager.Song.time = newTime;
        }

        int ThresholdIncreaseInput = 0;

        if(Input.GetKeyDown(KeyCode.K))
        {
            ThresholdIncreaseInput = -1;
        }
        else if(Input.GetKeyDown(KeyCode.L))
        {
            ThresholdIncreaseInput = 1;
        }
        if(ThresholdIncreaseInput != 0)
        {
            gameManager.ThresholdsMultiplier += ThresholdIncreaseInput * 0.25f;
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            musicManager.PauseMusic();
        }

        if(Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }

}
