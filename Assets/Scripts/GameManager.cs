﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour 
{
    #region Fields
    [SerializeField]
    private float perfectThreshold = 0.1f;
    [SerializeField]
    private float partialThreshold = 0.3f;
    [SerializeField]
    private float outOfControlTime = 0.2f;
    [SerializeField]
    private GameObject player = null;
    private Player playerScript;

    private HUDManager hudManager;

    private float thresholdsMultiplier = 1.0f;

    #endregion
    #region Properties
    public float ThresholdsMultiplier
    {
        get { return thresholdsMultiplier; }
        set
        {
            thresholdsMultiplier = value;
            hudManager.NoteTracker.gameObject.GetComponent<UINoteTracker>().ResizeZones();
        }
    }
    public float PerfectThreshold
    {
        get{return perfectThreshold * ThresholdsMultiplier; }
    }
    public float PartialThreshold
    {
        get{return partialThreshold * ThresholdsMultiplier; }
    }
    public GameObject PlayerGameObject
    {
        get{return player;}
    }
    public Player PlayerScript
    {
        get { return playerScript; }
    }
    public float OutOfControlTime
    {
        get{return outOfControlTime;}
    }
    #endregion
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<GameManager>();               
                //if you want this to be persistent through scenes try adding some non-destroy code here
            }
            return instance;
        }      
    }

    protected void Start()
    {
        hudManager = HUDManager.Instance;
        playerScript = player.GetComponent<Player>();
        playerScript.PlayerHealthComponent.LethalDamageTaken += LethalDamageTaken;
    }

    protected void LethalDamageTaken(object sender, LethalDamageTakenEventArgs e)
    {
        RestartLevel();
    }

    public static void DelayedFunction(float delay,Action action)
    {
        Instance.StartCoroutine(IEDelayedFunction(delay,action));
    }

    private static IEnumerator IEDelayedFunction(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);
        action();
    }

    public void PauseEditor()
    {
       // Time.timeScale = 0;
       // EditorApplication.isPaused = true;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
