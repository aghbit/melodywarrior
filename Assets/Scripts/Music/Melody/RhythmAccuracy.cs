
using System;
namespace AssemblyCSharp
{
    public enum RhythmAccuracy
    {
        Full = 0,
        Partial = 1,
        Miss = 2,
        Undefined = 3
    }
}

