using System;
namespace AssemblyCSharp
{
    public class HeldNote : Note
    {
        private float holdTime;
        private int numberOfLoops;
        private int loopIndex;
        public float HoldTime
        {
            get{return holdTime;}
            private set{holdTime = value;}
        }
        public int NumberOfLoops
        {
            get{return numberOfLoops;}
            private set{numberOfLoops = value;}
        }
        public int LoopIndex
        {
            get{return loopIndex;}
            private set{loopIndex = value;}
        }
        public HeldNote(float position, float nextNotePosition, float holdTime, int numberOfLoops = 1, int loopIndex = 0, bool isMajorNote = false) : base(position,nextNotePosition,isMajorNote)
        {
            HoldTime = holdTime;
            NumberOfLoops = numberOfLoops;
            LoopIndex = loopIndex;
        }
        
    }
}

