using System;
namespace AssemblyCSharp
{
    public interface IMelodyResponder
    {
        void OnMelody(Note note);
    }
}

