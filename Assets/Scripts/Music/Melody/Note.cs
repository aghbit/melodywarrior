using System;
namespace AssemblyCSharp
{
    public class Note
    {
        //position in timeline (in seconds)
        protected float position = 0;
        protected float nextNotePosition = 0;
        protected bool isMajorNote = false;
        public float Position
        {
            get{return position;}
            private set{position = value;}
        }

        public float NextNotePosition
        {
            get{return nextNotePosition;}
            private set{nextNotePosition = value;}
        }

        public float TimeToNextNote
        {
            get{return NextNotePosition-Position;}
        }

        public bool IsMajorNote
        {
            get{return isMajorNote;}
            private set{isMajorNote = value;}
        }

        public Note(float position, float nextNotePosition, bool isMajorNote = false)
        {
            Position = position;
            NextNotePosition = nextNotePosition;
            IsMajorNote = isMajorNote;

        }
    }
}

