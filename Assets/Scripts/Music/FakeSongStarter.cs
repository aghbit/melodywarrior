using UnityEngine;
using System;
namespace AssemblyCSharp
{
    public class FakeSongStarter : MonoBehaviour
    {
        [SerializeField]
        MusicParameters musicParameters = new MusicParameters();

        void Start()
        {
            GameManager.DelayedFunction(0,InitSong);
        }

        private void InitSong()
        {
            MusicManager manager = MusicManager.Instance;
            manager.ChangeMusicParameters(musicParameters);
        }
    }
}

