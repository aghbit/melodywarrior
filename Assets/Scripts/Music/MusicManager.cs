﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.Collections.Generic;
using System;

public class MusicManager : MonoBehaviour
{
    //everything should be stored in seconds (except BPM which is beats per minute)
    [SerializeField]
    MusicParameters startingMusicParameters = new MusicParameters();

    //this is bad but I just want to start when the action starts. Shouldn't be here once we get dedicated music
    [SerializeField]
    float minStartingTime = 21.5f;

    MusicParameters musicParameters;

    private AudioSource song;

    private List<Action> onMusicParamsChangedListeners = new List<Action>();

    public AudioSource Song
    {
        get { return song; }
        set { song = value; }
    }

    public float BPM
    {
        get{return musicParameters.BPM;}
    }

    public float BeatInterval
    {
        get{return 1.0f/(BPM/60.0f);}
    }

    public float Offset
    {
        get{return musicParameters.Offset;}
    }
    //1/16 means it will fire 16 times per beat. 
    //16 means it will fire every 16 beats.
    private readonly float[] availableBeatIntervals = new float[]{16.0f, 8.0f, 4.0f, 2.0f, 1.0f, 1.0f/2.0f,1.0f/4.0f,1.0f/8.0f,1.0f/16.0f};
    public static readonly float smallestBeatInterval = 1.0f/16.0f;
    public static readonly float biggestBeatInterval = 16;
    private readonly Dictionary<float,BeatTracker> beatTrackers = new Dictionary<float,BeatTracker>();

    private MelodyTracker melodyTrackerInstance;
    public MelodyTracker MelodyTrackerInstance
    {
        get{return melodyTrackerInstance;}
        private set{melodyTrackerInstance = value;}
    }
    public float SongLength
    {
        get{return GetComponent<AudioSource>().clip.length;}
    }

    private static MusicManager instance;
    public static MusicManager Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<MusicManager>();               
                //if you want this to be persistent through scenes try adding some non-destroy code here
            }
            return instance;
        }      
    }

    public void AddNewResponder(IBeatResponder newResponder, float wantedBeatInterval)
    {
        if(beatTrackers.ContainsKey(wantedBeatInterval))
        {
            beatTrackers[wantedBeatInterval].Add(newResponder);
        }
        else
        {
            Debug.LogError("BeatInterval invalid: " + wantedBeatInterval.ToString(),this);
        }
    }

    public void RemoveResponder(IBeatResponder oldResponder, float oldBeatInterval)
    {
        if(beatTrackers.ContainsKey(oldBeatInterval))
        {
            beatTrackers[oldBeatInterval].Remove(oldResponder);
        }
        else
        {
            Debug.LogError("BeatInterval invalid: " + oldBeatInterval.ToString(),this);
        }
    }

    public float ElapsedSongTime
    {
        get{return (float)(Song.timeSamples)/(float)(Song.clip.frequency);}
    }

    protected void Awake()
    {
        //the 180s should be changed when weapon is selected(change music parameters will be called)
        if(musicParameters == null)
        {
            musicParameters = startingMusicParameters;
        }


        foreach(float num in availableBeatIntervals)
        {
            beatTrackers.Add(num,new BeatTracker(180,num,Offset));
        }
        MelodyTrackerInstance = new MelodyTracker(180);
        Song = GetComponent<AudioSource>();
    }

	protected void Start () 
    {
        //audio.Play();
        //playMusicAfterDelay(3);
        //StartCoroutine(playMusicAfterDelay(3));

	}
	
    private IEnumerator playMusicAfterDelay(float delay)
    {
        //yield return new WaitForSeconds(delay);
        yield return 0;
        GetComponent<AudioSource>().Play();
    }

	// Update is called once per frame
	void Update () 
    {
        if(GetComponent<AudioSource>().isPlaying)
        {
            foreach(KeyValuePair<float,BeatTracker> kvp in beatTrackers)
            {
                kvp.Value.Update();
            }
            MelodyTrackerInstance.Update();
        }
        else
        {
            OnMusicEnd();
        }

	}

    private void OnMusicEnd()
    {

    }

    public void RegisterOnChangeMusicListener(Action ActionToCall)
    {
        onMusicParamsChangedListeners.Add(ActionToCall);
    }

    public void UnregisterOnChangeMusicListener(Action ActionToCall)
    {
        onMusicParamsChangedListeners.Remove(ActionToCall);
    }

    private void OnMusicParametersChanged()
    {
        foreach(Action act in onMusicParamsChangedListeners)
        {
            act();
        }
    }

    public void ChangeMusicParameters(MusicParameters newParameters)
    {
        musicParameters = newParameters;
        float timeInSong = ElapsedSongTime;
        GetComponent<AudioSource>().Stop();

        GetComponent<AudioSource>().clip = newParameters.Song;
        GetComponent<AudioSource>().Play();

        if(timeInSong < minStartingTime)
        {
            timeInSong = minStartingTime;
        }
            
        Song.time = (Song.clip.length > timeInSong) ? timeInSong : 0;

        foreach(KeyValuePair<float,BeatTracker> kvp in beatTrackers)
        {
            BeatTracker bt = kvp.Value;
            bt.ChangeMusicParameters(newParameters.BPM,newParameters.Offset);
        }
        if(newParameters.Beatmap!=null)
        {
            MelodyTrackerInstance.ChangeMusicParameters(newParameters.Beatmap);
        }
        else
        {
            MelodyTrackerInstance.ChangeMusicParameters(newParameters.BPM);
        }
        if(HUDManager.Instance!=null && HUDManager.Instance.NoteTracker != null)
        {
            HUDManager.Instance.NoteTracker.SendMessage("RecalculateNotes");
        }
           
        OnMusicParametersChanged();
    }

    //DEBUG:
    public void PauseMusic()
    {
        if(Song.isPlaying)
        {
            Song.Pause();
        }
        else
        {
            Song.UnPause();
        }
    }

}
