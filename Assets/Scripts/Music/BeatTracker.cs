using System;
using UnityEngine;
using System.Collections.Generic;
namespace AssemblyCSharp
{
    public class BeatTracker
    {
        private float BPM;
        private float offset;
        private float beatInterval;
        private float beatIntervalMultiplier;
        private float lastBeat = 0;
        private float LastBeat
        {
            get{return lastBeat;}
            set{lastBeat = value;}
        }
        private float beatCounter = 0;
        private List<IBeatResponder> responders;
        private MusicManager manager;
        private void fireBeatEvents(float time)
        {
            foreach(IBeatResponder responder in responders){
                responder.OnBeat(time, beatCounter);
            }
        }
        public BeatTracker(float BPM, float beatIntervalMultiplier, float offset, List<IBeatResponder> startingResponders)
        {
            this.BPM = BPM;
            this.offset = offset;
            this.beatIntervalMultiplier = beatIntervalMultiplier;
            beatInterval = (1/(BPM/60))*beatIntervalMultiplier;
            LastBeat = offset - beatInterval;
            if(startingResponders!=null)
                responders = startingResponders;
            else
                responders = new List<IBeatResponder>();
            manager = MusicManager.Instance;
        }
        public BeatTracker(float BPM, float beatIntervalMultiplier, float offset) : this(BPM,beatIntervalMultiplier,offset,null)
        {
        }

        public void ChangeMusicParameters(float newBPM,float newOffset)
        {
            BPM = newBPM;
            offset = newOffset;
            beatInterval = (1/(BPM/60))*beatIntervalMultiplier;
            LastBeat = offset - beatInterval;
            //TODO: check if beatcounter works properly
            beatCounter = 0;
            recalculateLastBeat();
        }

        private void recalculateLastBeat()
        {

            while(manager.ElapsedSongTime>LastBeat)//+beatInterval)
            {
                LastBeat += beatInterval;
                beatCounter += beatIntervalMultiplier;
            }
        }


        private void onBeat(float time)
        {
            fireBeatEvents(time);
            LastBeat += beatInterval;
            beatCounter += beatIntervalMultiplier;
            if(beatCounter>MusicManager.biggestBeatInterval)
            {
                beatCounter = beatIntervalMultiplier;
            }
        }
        public void Update()
        {
            if(manager.ElapsedSongTime>LastBeat+beatInterval)
            {
                onBeat(LastBeat+beatInterval);
            }
        }

        public void Add(IBeatResponder newResponder)
        {
            responders.Add(newResponder);
        }

        public void Remove(IBeatResponder oldResponder)
        {
            responders.Remove(oldResponder);
        }
    }
}

