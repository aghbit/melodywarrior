
using System;
namespace AssemblyCSharp
{
    public interface IBeatResponder
    {
        void OnBeat(float timeOfBeat, float beatInterval);
    }
}

