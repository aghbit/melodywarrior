using System;
using System.Collections;
using UnityEngine;
namespace AssemblyCSharp
{
    public class BasicWeapon : MonoBehaviour
    {
        protected class ShotInformation
        {
            public ShotInformation()
            {
                this.DamageInfo = null;
                this.HitAI = null;
            }
            public ShotInformation(DamageInstance DamageInfo, BasicAI HitAI)
            {
                this.DamageInfo = DamageInfo;
                this.HitAI = HitAI;
            }
            public DamageInstance DamageInfo = null;
            public BasicAI HitAI = null;
        }

        [SerializeField]
        protected float Damage = 5;
        [SerializeField]
        protected Transform BulletTrailPrefab = null;
       
        [SerializeField]
        protected LayerMask whatToHit;

        [SerializeField]
        MusicParameters musicParameters = null;


        [SerializeField]
        protected float range = 100;

        protected MusicManager musicManager;
        protected Transform firePoint;
        protected GameManager gameManager;
        public void Awake () 
        {
            firePoint = transform.Find("FirePoint");
            musicManager = MusicManager.Instance;
            if(firePoint == null)
            {
                Debug.LogError("No firepoint found for weapon");
            }
        }

        protected virtual void Start()
        {
            gameManager = GameManager.Instance;
        }

        protected virtual void Shoot(ShotInformation ShotInfo = null)
        {
            if(ShotInfo == null)
            {
                ShotInfo = new ShotInformation();
            }
            Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            Vector2 firePointPosition = new Vector2(firePoint.position.x,firePoint.position.y);
            RaycastHit2D hit = Physics2D.Raycast(firePointPosition,(mousePosition-firePointPosition).normalized,range,whatToHit);
            Effect();

            if(hit.collider != null)
            {
                DamageInstance DamageInfo = new DamageInstance(Damage, GameManager.Instance.PlayerGameObject);
                ShotInfo.DamageInfo = DamageInfo;
                hit.collider.SendMessage("TakeDamage", DamageInfo, SendMessageOptions.DontRequireReceiver);
                BasicAI HitEnemy = hit.collider.gameObject.GetComponent(typeof(BasicAI)) as BasicAI;

                if (HitEnemy)
                {
                    ShotInfo.HitAI = HitEnemy;
                    OnEnemyHit(ShotInfo);
                }
            }
        }

        protected void Effect()
        {
            Instantiate(BulletTrailPrefab,firePoint.position,firePoint.rotation);
        }

        private IEnumerator SelectOnFirstFrame()
        {
            yield return null;
            OnSelect();
        }


        public virtual void OnSelect()
        {
            musicManager.ChangeMusicParameters(musicParameters);
        }

        public virtual void OnDeselect()
        {

        }

        protected virtual void OnEnemyHit(ShotInformation ShotInfo)
        {

        }
    }
}

