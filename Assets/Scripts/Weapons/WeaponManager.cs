using UnityEngine;
using System;
namespace AssemblyCSharp
{
    public class WeaponManager : MonoBehaviour
    {
        enum WeaponSlot { Primary, Secondary, Tertiary};

        [SerializeField]
        private GameObject primaryWeapon = null;
        [SerializeField]
        private GameObject secondaryWeapon = null;
        [SerializeField]
        private GameObject tertiaryWeapon = null;
        [SerializeField]
        private WeaponSlot startingWeaponSlot = WeaponSlot.Primary;

        private GameObject chosenWeapon;

        protected void Start()
        {
            GameManager.DelayedFunction(0,InitWeapons);
        }

        private void InitWeapons()
        {
            primaryWeapon.SetActive(false);
            secondaryWeapon.SetActive(false);
            tertiaryWeapon.SetActive(false);
            switch(startingWeaponSlot)
            {
                case WeaponSlot.Primary:
                    chosenWeapon = primaryWeapon;
                    break;
                case WeaponSlot.Secondary:
                    chosenWeapon = secondaryWeapon;
                    break;
                case WeaponSlot.Tertiary:
                    chosenWeapon = tertiaryWeapon;
                    break;
            }
            chosenWeapon.SetActive(true);
            chosenWeapon.SendMessage("OnSelect");
        }

        protected void Update()
        {
            if(Input.GetButtonDown("PrimaryWeapon") && primaryWeapon != null)
            {
                TryChoosingWeapon(primaryWeapon);
            }
            else if(Input.GetButtonDown("SecondaryWeapon") && secondaryWeapon != null)
            {
                TryChoosingWeapon(secondaryWeapon);
            }
            else if(Input.GetButtonDown("TertiaryWeapon") && tertiaryWeapon != null)
            {
                TryChoosingWeapon(tertiaryWeapon);
            }
        }

        private void TryChoosingWeapon(GameObject weap)
        {
            if(chosenWeapon == weap)
            {
                return;
            }
            chosenWeapon.SendMessage("OnDeselect");
            chosenWeapon.SetActive(false);
            chosenWeapon = weap;
            weap.SetActive(true);
            weap.SendMessage("OnSelect");
        }

    }
}

