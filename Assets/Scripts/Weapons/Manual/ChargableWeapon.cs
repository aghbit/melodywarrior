using System;
using UnityEngine;
using System.Collections.Generic;
namespace AssemblyCSharp
{
    public class ChargableWeapon : ManualWeapon
    {
        protected class ChargableShotInformation : ShotInformation
        {
            public ChargableShotInformation(DamageInstance DamageInfo, BasicAI HitAI, RhythmAccuracy RhythmResult)
                : base(DamageInfo, HitAI)
            {
                this.RhythmResult = RhythmResult;
            }
            public ChargableShotInformation()
                : base()
            {
                RhythmResult = RhythmAccuracy.Undefined;
            }
            public RhythmAccuracy RhythmResult;
        }

        [SerializeField]
        protected float minPower = 10.0f;

        [SerializeField]
        protected float maxPower = 100.0f;

        protected float CurrentPower { get; set; }
        
        [SerializeField]
        protected float[] shootingCosts = { 0.0f, 0.0f, 5.0f };
        [SerializeField]
        protected float[] chargeValues = { 15.0f, 5.0f, 0.0f };

        protected override void Start()
        {
            base.Start();

            CurrentPower = minPower;
            UpdateHudAmmo();
        }

        protected override void Update()
        {
            if(Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2"))
            {
                TryShooting();
                CropPower();
                UpdateHudAmmo();
            }
        }

        protected override void TryShooting()
        {
            RhythmAccuracy acc = melodyModule.Activate();
            CurrentPower -= shootingCosts[(int)acc];
            hudManager.ShotRating.SendMessage("CheckAccuracyDisplay", acc);
            if(shootingCosts[(int)acc] != 0)
            {
                UpdateHudAmmo();
            }
            
            if (acc != RhythmAccuracy.Miss)
            {
                Shoot(new ChargableShotInformation(null, null, acc));
            }
            
        }

        protected void CropPower()
        {
            if(CurrentPower<minPower)
            {
                CurrentPower = minPower;
            }
            else if(CurrentPower>maxPower)
            {
                CurrentPower = maxPower;
            }
        }
        
        public override void OnSelect()
        {
            base.OnSelect();
            UpdateHudAmmo();
        }

        protected void UpdateHudAmmo()
        {
            hudManager.AmmoDisplay.SendMessage("SetDisplayedAmmo",CurrentPower);
        }

        protected override void OnEnemyHit(ShotInformation ShotInfo)
        {
            ChargableShotInformation ChargeShotInfo = ShotInfo as ChargableShotInformation;
            CurrentPower += chargeValues[(int)ChargeShotInfo.RhythmResult];
            UpdateHudAmmo();
        }
    }
}

