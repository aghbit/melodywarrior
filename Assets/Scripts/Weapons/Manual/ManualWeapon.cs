using System;
using UnityEngine;
namespace AssemblyCSharp
{
    public class ManualWeapon : BasicWeapon
    {
        protected MelodyTracker melodyTracker;

        protected HUDManager hudManager;
        protected Note closestNote;
        protected Note ClosestNote
        {
            get {return closestNote;}
            set {closestNote = value;}
        }
        protected MelodyModule melodyModule;
        protected override void Start()
        {
            base.Start();
            musicManager = MusicManager.Instance;
            melodyTracker = musicManager.MelodyTrackerInstance;
            hudManager = HUDManager.Instance;
            melodyModule = new MelodyModule();
        }
        
        protected virtual void Update()
        {
            if (Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2"))
            {
                TryShooting();
            }
        }

        protected virtual void TryShooting()
        {
            RhythmAccuracy acc = melodyModule.Activate();
            Shoot();
            hudManager.ShotRating.SendMessage("CheckAccuracyDisplay", acc);
        }
    }
}

