﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.Collections.Generic;

public class MultiShotChargableWeapon : ChargableWeapon
{
    protected class MultipleShotInformation : ChargableShotInformation
    {
        public MultipleShotInformation(DamageInstance DamageInfo, BasicAI HitAI, RhythmAccuracy RhythmResult)
            : base(DamageInfo, HitAI, RhythmResult)
        {
            this.RhythmResult = RhythmResult;
        }
        public MultipleShotInformation()
            : base()
        {

        }
        public List<BasicAI> AllHitAIs = new List<BasicAI>();
    }

    protected MultipleShotInformation PrepareShotInformation(ShotInformation ShotInfo)
    {
        if(ShotInfo == null)
        {
            return new MultipleShotInformation();
        }
        if(ShotInfo is MultipleShotInformation)
        {
            return (MultipleShotInformation)ShotInfo;
        }
        MultipleShotInformation ShotInfoToReturn = new MultipleShotInformation();
        ShotInfoToReturn.DamageInfo = ShotInfo.DamageInfo;
        ShotInfoToReturn.HitAI = ShotInfo.HitAI;
        ChargableShotInformation ChargableShotInfo = ShotInfo as ChargableShotInformation;
        if(ChargableShotInfo != null)
        {
            ShotInfoToReturn.RhythmResult = ChargableShotInfo.RhythmResult;
        }
        return ShotInfoToReturn;
    }

    protected override void OnEnemyHit(ShotInformation ShotInfo)
    {
        MultipleShotInformation MultiShotInfo = ShotInfo as MultipleShotInformation;
        if(MultiShotInfo != null)
        {
            CurrentPower += chargeValues[(int)MultiShotInfo.RhythmResult] * MultiShotInfo.AllHitAIs.Count;
            UpdateHudAmmo();
        }
        else
        {
            base.OnEnemyHit(ShotInfo);
        }
        
    }
}
