using UnityEngine;
using System;
using System.Collections.Generic;


namespace AssemblyCSharp
{
    public class SniperRifle : MultiShotChargableWeapon
    {
        [SerializeField]
        protected LayerMask blockingLayer;
        [SerializeField]
        protected float BaseDamage = 20;
        [SerializeField]
        protected float ChargeDamageMultiplier = 0.5f;
        //pierces enemies but not walls
        protected override void Shoot(ShotInformation ShotInfo = null)
        {
            MultipleShotInformation MultiShotInfo = PrepareShotInformation(ShotInfo);
            Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            Vector2 firePointPosition = new Vector2(firePoint.position.x,firePoint.position.y);
            RaycastHit2D[] hits = Physics2D.RaycastAll(firePointPosition,(mousePosition-firePointPosition).normalized,range,whatToHit);
            
            List<RaycastHit2D> hitsList = new List<RaycastHit2D>(hits);

            hitsList.Sort((h1, h2) => h1.distance.CompareTo(h2.distance));

            Vector2 hitPoint = (Vector2)firePoint.position + (Vector2)((mousePosition-firePointPosition).normalized * range);
            DamageInstance DamageInfo = new DamageInstance(Damage, GameManager.Instance.PlayerGameObject);
            MultiShotInfo.DamageInfo = DamageInfo;
            for (int i = 0; i<hitsList.Count;i++)
            {
                RaycastHit2D hit = hitsList[i];
                if(hit.collider != null)
                {
                    hit.collider.SendMessage("TakeDamage",DamageInfo,SendMessageOptions.DontRequireReceiver);
                    //we've hit a platform. Bullet shouls stop
                    if(((1<<hit.collider.gameObject.layer) & blockingLayer) != 0)
                    {
                        hitPoint = hit.centroid;
                        break;
                    }
                    BasicAI HitEnemy = hit.collider.gameObject.GetComponent(typeof(BasicAI)) as BasicAI;
                    if (HitEnemy)
                    {
                        MultiShotInfo.HitAI = HitEnemy;
                        MultiShotInfo.AllHitAIs.Add(HitEnemy);
                        
                    }
                }

            }
            if(MultiShotInfo.AllHitAIs.Count != 0)
            {
                OnEnemyHit(MultiShotInfo);
            }
            DrawLineToPoint(hitPoint);
        }

        protected override void TryShooting()
        {
            Damage = BaseDamage + CurrentPower*ChargeDamageMultiplier;
            base.TryShooting();
        }


        private void DrawLineToPoint(Vector2 destination)
        {
            Vector2 start = firePoint.position;
            float length = (destination-start).magnitude;
            Transform line = (Transform)Instantiate(BulletTrailPrefab,firePoint.position,firePoint.rotation);
            Vector3 s = line.localScale;
            s.x = length;
            line.localScale = s;
        }

    }
}

