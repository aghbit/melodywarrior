
using System;
namespace AssemblyCSharp
{
    public class MelodyModule : IMelodyResponder, IRhythmAccuracyModule
    {
        protected MelodyTracker melodyTracker;
        protected MusicManager musicManager;
        protected GameManager gameManager;
        protected Note lastNote;
        protected Note LastNote
        {
            get {return lastNote;}
            set {lastNote = value;}
        }
        private bool lastNoteUsed = false;
        private bool nextNoteUsed = false;
        public MelodyModule()
        {
            musicManager = MusicManager.Instance;
            melodyTracker = musicManager.MelodyTrackerInstance;
            gameManager = GameManager.Instance;
            melodyTracker.Add(this);
            ChangeNote(melodyTracker.NextNote);
        }

        protected void ChangeNote(Note newNote)
        {
            LastNote = newNote;
            lastNoteUsed = nextNoteUsed;
            nextNoteUsed = false;
        }
        public void OnMelody(Note note)
        {
            ChangeNote(note);
        }
        
        protected float GetTimeDifference(Note note)
        {
            return Math.Abs(note.Position-musicManager.ElapsedSongTime);
        }
        
        protected bool IsNoteTimeValid(Note note)
        {
            if(GetTimeDifference(note)>gameManager.PartialThreshold)
            {
                return false;
            }
            
            return true;
        }
        
        protected Note GetFirstValidNote()
        {
            if(IsNoteTimeValid(LastNote) && !lastNoteUsed)
            {
                lastNoteUsed = true;
                return LastNote;
            }
            if(IsNoteTimeValid(melodyTracker.NextNote) && !nextNoteUsed)
            {
                nextNoteUsed = true;
                return melodyTracker.NextNote;
            }
            return null;
        }

        public RhythmAccuracy Activate()
        {
            Note shootNote = GetFirstValidNote();
            if(shootNote == null || GetTimeDifference(shootNote)>gameManager.PartialThreshold)
            {
                return RhythmAccuracy.Miss;
            }
            else if(GetTimeDifference(shootNote)<gameManager.PerfectThreshold)
            {
                return RhythmAccuracy.Full;
            }
            else
            {
                return RhythmAccuracy.Partial;
            }
        }
    }
}

