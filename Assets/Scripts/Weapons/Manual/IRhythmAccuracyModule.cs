
using System;
namespace AssemblyCSharp
{
    public interface IRhythmAccuracyModule
    {
        RhythmAccuracy Activate();
    }
}

