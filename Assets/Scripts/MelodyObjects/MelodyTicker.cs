using System;
using UnityEngine;
namespace AssemblyCSharp
{
    public class MelodyTicker : MonoBehaviour, IMelodyResponder
    {
        MelodyTracker melodyTracker;
        void Start()
        {
            melodyTracker = MusicManager.Instance.MelodyTrackerInstance;
            melodyTracker.Add(this);
        }
        public void OnMelody(Note note)
        {
            GetComponent<AudioSource>().Play();
        }
    }
}

