﻿using AssemblyCSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
	public event EventHandler<DamageTakenEventArgs> DamageTaken;
    public event EventHandler<LethalDamageTakenEventArgs> LethalDamageTaken;
    public bool DestroyOnDeath = true;
    [SerializeField]
    protected float health = 100;
    public float Health
    {
        get
        {
            return health;
        }
        private set
        {
            health = value;
        }
    }
    [SerializeField]
    private float invulnerabilityTime = 1.0f;

    public float InvulnerabilityTime
    {
        get { return invulnerabilityTime; }
    }
    
    [SerializeField]
    private bool canBeInvulnerable = false;
    
    public bool CanBeInvulnerable
    {
        get
        {
            return canBeInvulnerable;
        }
        set
        {
            canBeInvulnerable = value;
        }
    }

    public bool IsInvulnerable { get; set; }

    public virtual void TakeDamage(DamageInstance DamageInfo)
    {
        if(!IsInvulnerable)
        {
            Health -= DamageInfo.Damage;

            if (CanBeInvulnerable && DamageInfo.TriggersInvulnerability)
            {
                IsInvulnerable = true;
                GameManager.DelayedFunction(InvulnerabilityTime, () => IsInvulnerable = false);
            }

            DamageTakenEventArgs DamageArgs = new DamageTakenEventArgs();
            DamageArgs.DamageInfo = DamageInfo;
            DamageArgs.NewHealth = Health;
            OnDamageTaken(DamageArgs);
            if (Health <= 0)
            {
                LethalDamageTakenEventArgs LethalDamageArgs = new LethalDamageTakenEventArgs();
                LethalDamageArgs.DamageInfo = DamageInfo;
                LethalDamageArgs.NewHealth = Health;
                OnLethalDamageTaken(LethalDamageArgs);
                if(DestroyOnDeath)
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    protected virtual void OnDamageTaken(DamageTakenEventArgs e)
    {
        if(DamageTaken != null)
        {
            DamageTaken(this, e);
        }
    }
    protected virtual void OnLethalDamageTaken(LethalDamageTakenEventArgs e)
    {
        if(LethalDamageTaken != null)
        {
            LethalDamageTaken(this, e);
        }
    }
}

public class DamageTakenEventArgs : EventArgs
{
    public DamageInstance DamageInfo { get; set; }
    public float NewHealth { get; set; }
}

public class LethalDamageTakenEventArgs : DamageTakenEventArgs
{

}
