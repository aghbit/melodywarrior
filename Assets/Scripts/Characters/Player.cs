﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System;

[RequireComponent(typeof(HealthComponent))]
public partial class Player : MonoBehaviour
{
    #region Fields
    private bool isFacingRight;
    private CharacterController2D controller;
    private float normalizedHorizontalSpeed;

    public float MaxSpeed = 8;
    public float SpeedAccelerationOnGround = 10f;
    public float SpeedAccelerationInAir = 5f;

    public Transform playerGraphics;

    private Animator anim;

    private GameManager gameManager;

    private HUDManager hudManager;

    private HealthComponent healthComponent;

    public HealthComponent PlayerHealthComponent
    {
        get
        {
            return healthComponent;
        }
        private set
        {
            healthComponent = value;
        }
    }

    #endregion
    #region Properties

    private bool hasControl = true;

    private bool HasControl
    {
        get{return hasControl;}
        set{hasControl = value;}
    }
    #endregion
    protected void Awake()
    {
        playerGraphics = transform.Find("Graphics"); 
        anim = GetComponent<Animator>();
        PlayerHealthComponent = GetComponent<HealthComponent>();
    }

    protected void Start()
    {
        controller = GetComponent<CharacterController2D>();
        isFacingRight = playerGraphics.localScale.x > 0;

        gameManager = GameManager.Instance;
        hudManager = HUDManager.Instance;
        PlayerHealthComponent.DamageTaken += DamageTaken;
    }

    protected void Update()
    {
        HandleAnim();
        if(HasControl)
        {
            HandleInput();
            float movementFactor = controller.State.IsGrounded ? SpeedAccelerationOnGround : SpeedAccelerationInAir;
            controller.SetHorizontalForce(Mathf.Lerp(controller.Velocity.x,normalizedHorizontalSpeed*MaxSpeed,Time.deltaTime * movementFactor));

        }
        JetpackUpdate();
    }

    private void HandleAnim()
    {
        anim.SetBool("Ground", controller.State.IsGrounded);
        anim.SetFloat("Speed", Mathf.Abs(controller.Velocity.x));
    }

    private void HandleInput()
    {
        if(Input.GetKey(KeyCode.D))
        {
            normalizedHorizontalSpeed = 1;
            if(!isFacingRight)
            {
                Flip();
            }
        }
        else if (Input.GetKey(KeyCode.A))
        {
            normalizedHorizontalSpeed = -1;
            if(isFacingRight)
            {
                Flip();
            }
        }
        else
        {
            normalizedHorizontalSpeed = 0;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(controller.CanJump)
            {
                controller.Jump();
                GameManager.DelayedFunction(DefaultJetpackParameters.HoldSpaceJetpackDelay, () =>
                {
                    if(Input.GetKey(KeyCode.Space))
                    {
                        StartJetpack();
                    }
                });
            }
            else
            {
                StartJetpack();
            }
        }
        else if(!Input.GetKey(KeyCode.Space))
        {
            StopJetpack();
        }
    }

    private void Flip()
    {
        playerGraphics.localScale = new Vector3(-playerGraphics.localScale.x, playerGraphics.localScale.y,playerGraphics.localScale.z);
        isFacingRight = playerGraphics.localScale.x > 0;
    }

    protected void DamageTaken(object sender, DamageTakenEventArgs e)
    {
        if (e.DamageInfo.TriggersInvulnerability)
        {
            StartCoroutine(BlinkingMode(PlayerHealthComponent.InvulnerabilityTime));
        }
        if (e.DamageInfo.TriggersKnockback)
        {
            if (e.DamageInfo.SpecialKnockbackBehaviour != null)
            {
                e.DamageInfo.SpecialKnockbackBehaviour(this);
                return;
            }
            HasControl = false;

            Action regainControl = () =>
            {
                HasControl = true;
            };

            DefaultKnockback(e.DamageInfo);

            GameManager.DelayedFunction(gameManager.OutOfControlTime, regainControl);
        }
    }

    //todo: handle enemies on top of player
    private void DefaultKnockback(DamageInstance damageInstance)
    {
        float strength = damageInstance.KnockBackStrength;
        controller.SetHorizontalForce(-strength*0.5f);
        controller.SetVerticalForce(strength);
    }

    private IEnumerator BlinkingMode(float blinkingTime)
    {
        float singleBlinkTime = 0.1f;
        bool isVisible = true;
        while(blinkingTime>0)
        {
            isVisible = !isVisible;
            setVisibilityRecursively(isVisible);

            blinkingTime -= singleBlinkTime;
            yield return new WaitForSeconds(singleBlinkTime);
        }
        setVisibilityRecursively(true);
    }

    private void setVisibilityRecursively(bool isVisible)
    {
        if(GetComponent<Renderer>()!=null)
            GetComponent<Renderer>().enabled = isVisible;
        foreach(Transform child in transform)
        {
            if(child.GetComponent<Renderer>() != null)
                child.GetComponent<Renderer>().enabled = isVisible;
        }
    }

}
