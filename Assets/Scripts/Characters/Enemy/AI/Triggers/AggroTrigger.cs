﻿using UnityEngine;
using System.Collections;

public class AggroTrigger : MonoBehaviour
{
    private bool isCollidingWithPlayer = false;
    protected void OnTriggerEnter2D(Collider2D other)
    {
        if(!isCollidingWithPlayer&&other.tag == "Player")
        {
            isCollidingWithPlayer = true;
            transform.parent.SendMessage("OnPlayerEnterAggro",other.transform);
        }
    }

    protected void OnTriggerExit2D(Collider2D other)
    {
        if(isCollidingWithPlayer&&other.tag == "Player")
        {
            isCollidingWithPlayer = false;
            transform.parent.SendMessage("OnPlayerExitAggro",other.transform);
        }
    }
}
