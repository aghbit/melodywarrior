using System;
using UnityEngine;


public class DamageTrigger : MonoBehaviour
{
    protected bool isCollidingWithPlayer = false;
    protected void OnTriggerEnter2D(Collider2D other)
    {
        if(!isCollidingWithPlayer&&other.tag == "Player")
        {
            isCollidingWithPlayer = true;
            transform.parent.SendMessage("OnPlayerEnterDamage",other.transform);
        }
    }

    protected void OnTriggerExit2D(Collider2D other)
    {
        if(isCollidingWithPlayer&&other.tag == "Player")
        {
            isCollidingWithPlayer = false;
            transform.parent.SendMessage("OnPlayerExitDamage",other.transform);
        }
    }
    //for now this is probably going to get fired twice, but damage cooldown should work, so w/e
    protected void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            transform.parent.SendMessage("OnPlayerStayDamage",other.transform);
        }
    }
}
