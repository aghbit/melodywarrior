﻿using UnityEngine;
using System.Collections;

public class FightingTrigger : MonoBehaviour
{
    protected bool isCollidingWithPlayer = false;
    protected void OnTriggerEnter2D(Collider2D other)
    {
        if(!isCollidingWithPlayer&&other.tag == "Player")
        {
            isCollidingWithPlayer = true;
            transform.parent.SendMessage("OnPlayerEnterFighting",other.transform);
        }
    }

    protected void OnTriggerExit2D(Collider2D other)
    {
        if(isCollidingWithPlayer&&other.tag == "Player")
        {
            isCollidingWithPlayer = false;
            transform.parent.SendMessage("OnPlayerExitFighting",other.transform);
        }
    }
}
