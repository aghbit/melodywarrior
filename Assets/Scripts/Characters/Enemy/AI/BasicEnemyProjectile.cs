using UnityEngine;
using System;
namespace AssemblyCSharp
{
    //TODO: pooling instead of spawning new projectiles

    public class BasicEnemyProjectile : MonoBehaviour
    {
        [SerializeField]
        protected float speed = 10;
        [SerializeField]
        protected float damage = 1;
        [SerializeField]
        protected float lifeTime = 10;
        [SerializeField]
        private LayerMask layerToIgnore = 0;


        protected float destroyTime = 0;

        private Vector2 direction;

        new protected BoxCollider2D collider;

        private GameObject owner;

        public GameObject Owner
        {
            get{return owner;}
            set{owner = value;}
        }

        public Vector2 Direction
        {
            get{return direction;}
            set{direction = value.normalized;}
        }

        protected void Awake()
        {
            destroyTime = Time.realtimeSinceStartup + lifeTime;
            collider = GetComponent<BoxCollider2D>();
        }

        protected void Update()
        {
            if(Time.realtimeSinceStartup > destroyTime)
            {
                TryDestroy();
                return;
            }
            Move();
        }

        protected virtual void Move()
        {
            //TODO: cast ray to make sure we dont miss player
            Vector2 deltaDir = Direction * speed * Time.deltaTime;
            transform.position += (Vector3)deltaDir;
        }

        protected virtual void OnPlayerHit(GameObject player)
        {
            player.SendMessage("TakeDamage",new DamageInstance(damage,gameObject,Owner));
        }

        public virtual void OnCollisionEnter2D(Collision2D coll)
        {
            Collider2D other = coll.collider;
            if(other.tag == "Player")
            {
                OnPlayerHit(other.gameObject);
                TryDestroy();
            }
            else if(coll.collider.gameObject.layer != layerToIgnore)
            {
                TryDestroy();
            }
        }

        protected void TryDestroy()
        {
            Destroy(gameObject);
        }

    }
}

